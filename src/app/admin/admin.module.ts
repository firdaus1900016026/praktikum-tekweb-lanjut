import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { DasboardComponent } from './dasboard/dasboard.component';
import { RouterModule, Routes } from '@angular/router';
import { MaterialDesign } from '../material/material.module';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path:'',
    component:AdminComponent,
    children:[
      {
        path:'dasboard',
        component:DasboardComponent
      },
      {
        path:'product',
        component:ProductComponent
      },
      {
        path:'',
        redirectTo:'/admin/dasboard',
        pathMatch:'full'
      }
    ]
  }
]

@NgModule({
  declarations: [AdminComponent, DasboardComponent, ProductComponent, ProductDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MaterialDesign,
    FormsModule
  ]
})
export class AdminModule { }
